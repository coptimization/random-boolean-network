# RBN
Random Boolean Network C++

This implementation uses adjacency matrix. A RBN representation for the boolean network.
The connections_graph it's the connection matrix
Vector it's the logic vector matrix for connections.
H_States it's a historic matrix for comparison between to networks.
Vertex_Node it's the networks node state.

Prob_Graph it's a probability matrix between 0.0 and 1.0. Still working on that.

## Getting started

### Prerequisites
To run this plugin you need to have it installed on your computer:

[Cpplint](https://github.com/cpplint/cpplint) -  Static code checker for C++

[Cmake](https://cmake.org) - Open-source, cross-platform family of tools designed to build, test and package software.

### Installation

Downloading the repository.

git clone https://gitlab.com/coptimization/random-boolean-network.git


After cloning the repository it is necessary to run the commands below to update cpplint submodules.

git submodule sync --recursive
git submodule update --init --recursive

After all done.

## Authors

* *Rita Alamino* - Desenvolvidora - [ritaalamino](https://gitlab.com/ritaalamino)
* *Thiago Jansen Sampaio* - Colaborador - [yABSampaio](https://gitlab.com/yABSampaio)

See list of all project contributors [Contributors](https://gitlab.com/coptimization/random-boolean-network/-/project_members).

## Licença
This project is licensed under the MIT license - see [LICENSE.md] (LICENSE.md) for more details.